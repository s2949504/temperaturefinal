package nl.utwente.di.temperature;

public class Temperature {
    public double getTemperature(String isbn) {
        return Double.parseDouble(isbn)*1.8 + 32;
    }
}
