package nl.utwente.di.temperature;

import nl.utwente.di.temperature.Temperature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTemp {
    /** * Test the Quoter */
    @Test
    public void testTemp1() throws Exception {
        Temperature tempC = new Temperature();
        double  tempF = tempC.getTemperature("1");
        Assertions.assertEquals(33.8, tempF, 0.0, "Temperature in Fahrenheit");
    }
}
